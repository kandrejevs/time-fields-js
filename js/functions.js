/**
 * Created by kriss on 27.12.14.
 */

/**
 * Hide all text area fields and show only those which are within range
 */
function showHideTimeFields() {
    var timeArray = {
        0: '',
        1: '.viens',
        2: '.divi',
        3: '.tris',
        4: '.cetri',
        5: '.pieci',
        6: '.sesi',
        7: '.septini',
        8: '.astoni',
        9: '.devini',
        10: '.desmit',
        11: '.vienpadsmit',
        12: '.divpadsmit',
        13: '.trispadsmit',
        14: '.cetrapadsmit',
        15: '.piecpadsmit',
        16: '.sespadsmit',
        17: '.septinpadsmit',
        18: '.astonpadsmit',
        19: '.devinpadsmit',
        20: '.divdesmit',
        21: '.divdesmitviens',
        22: '.divdesmitdivi',
        23: '.divdesmittris',
        24: '.divdesmitcetri'
    };

    var startTime = $('select[name="laiks"]').val(),
        endTime = $('select[name="diena"]').val();

    //hide all textarea fields
    $.each(timeArray, function (key, value) {
        $(value).hide();
    });

    //show only textareas within selected range
    $.each(timeArray, function (key, value) {
        if (parseInt(key) > startTime && parseInt(key) <= endTime) {
            $(value).show(200);
        }
    });
}

/**
 * function for counting total hours depending on start and end time
 */
function countTotalHours() {
    var startTime = $('select[name="laiks"]').val(),
        endTime = $('select[name="diena"]').val(),
        hours = endTime - startTime;

    $('input[name="stundas"]').val(hours);
}


/**
 * Refresh text area fields on time change
 */
function refreshOnSelectChange() {
    var startTime = $('select[name="laiks"]'),
        endTime = $('select[name="diena"]');

    startTime.on('change', function () {
        showHideTimeFields();
        countTotalHours();
    });

    endTime.on('change', function () {
        showHideTimeFields();
        countTotalHours();
    });
}

/**
 * Init functions
 */
$(document).ready( function () {
    showHideTimeFields();
    refreshOnSelectChange();
    countTotalHours();
});




